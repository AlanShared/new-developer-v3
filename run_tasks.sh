#!/bin/bash

echo "=============== Executing task4.py ==============="
python /usr/src/app/task4.py

echo "=============== Executing Unittest for task4.py ==============="
python -m unittest /usr/src/app/task4_utest.py
