from task4 import find_matching_pair
import unittest

class TestFindMatchingPair(unittest.TestCase):
    def test_matching_pair(self):
        # Test case with a matching pair
        numbers1 = [2, 3, 6, 7]
        target_sum1 = 9
        self.assertEqual(find_matching_pair(numbers1, target_sum1), "OK, matching pair: (2, 7)")

        # Test case with no matching pair
        numbers2 = [1, 3, 3, 7]
        target_sum2 = 9
        self.assertIsNone(find_matching_pair(numbers2, target_sum2))

    def test_edge_cases(self):
        # Test with an empty list
        numbers3 = []
        target_sum3 = 10
        self.assertIsNone(find_matching_pair(numbers3, target_sum3))

        # Test with a single element list
        numbers4 = [5]
        target_sum4 = 5
        self.assertIsNone(find_matching_pair(numbers4, target_sum4))

        # Test with duplicate elements
        numbers5 = [1, 2, 2, 3, 4, 5]
        target_sum5 = 4
        self.assertEqual(find_matching_pair(numbers5, target_sum5), "OK, matching pair: (1, 3)")

if __name__ == '__main__':
    unittest.main()
