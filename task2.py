# CI Automation Script in Python

def build():
    # Simulate build process
    print("Building...")


def test():
    # Simulate running tests
    print("Running tests...")


def deploy():
    # Simulate deployment process
    print("Deploying...")


if __name__ == "__main__":
    # Entry point
    build()
    test()
    deploy()
