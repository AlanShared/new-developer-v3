## RTFM and LMGTFY Usage
- Last time I used RTFM: [Today, while reviewing these exercises]
- Last time I used LMGTFY: [Today, when searching for the meaning of RTFM and LMGTFY ;) ]

## Operating System
- **OS:** 
  - [Linux] 
  - [Windows]

## Programming Languages
- **Languages mastered:**
  - [Python]
  - [HTML]
  - [SQL]
  - [Java]
  - [PHP]
