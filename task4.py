def find_matching_pair(numbers, target_sum):
    """
    Finds a matching pair in a sorted list of numbers that sums up to the target sum.

    Args:
    - numbers (list): A sorted list of numbers.
    - target_sum (int): The target sum to find.

    Returns:
    - tuple: A tuple containing the matching pair if found, otherwise returns None.
    """
    # Initialize pointers
    left_pointer = 0
    right_pointer = len(numbers) - 1    # Point to the last element

    # Iterate through the list
    while left_pointer < right_pointer:
        current_sum = numbers[left_pointer] + numbers[right_pointer]
        if current_sum == target_sum:
            return f"OK, matching pair: ({numbers[left_pointer]}, {numbers[right_pointer]})"
        elif current_sum < target_sum:
            left_pointer += 1   # Move the left pointer to the right
        else:
            right_pointer -= 1  # Move the right pointer to the left
    # If no matching pair is found
    return None


# Example usage:
numbers1 = [2, 3, 6, 7]
target_sum1 = 9
print(find_matching_pair(numbers1, target_sum1))  # Output: Ok, matching pair: (2, 7)

numbers2 = [1, 3, 3, 7]
target_sum2 = 5
print(find_matching_pair(numbers2, target_sum2))  # Output: None
