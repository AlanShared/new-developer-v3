FROM python:3.9-slim
LABEL authors="Alan Meneses"

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy content files into the container at /usr/src/app
COPY . .

# Copy the script into the container
COPY run_tasks.sh .

# Make the script executable
RUN chmod +x run_tasks.sh

# Run the script when the container starts
CMD ["./run_tasks.sh"]
